# Tuto git

## Git quésaco ?

Git est un outil de gestion de version permettant de facilement partager du code. Git est fait pour sauvegarder de nombreuses versions d'un même fichier de manière optimale pour pouvoir connaitre les différences entre deux versions d'un programme ou pouvoir retrouver un bout de code supprimé il y a quelque temps.

### Un peu de pratique

Git est un programme qui ne travaille que dans un environment tres précis : *un repository* ou *repo*. Cela peut paraitre complexe mais tout se passe de manière assez simple car git se charge de gérer tout cela.

Commençons par créer un repo : On va quelque part dans notre ordinateur et on crée un dossier puis on initialise git.
```
$ cd /tmp/
$ mkdir mon-repo-git
$ cd mon-repo-git
$ git init
```
Toutes les commandes en lien avec git ont la forme par `git ...` On peut maintenant voir ce qu'il se passe dans ce repo git :
```
$ git status
On branch master

No commits yet

nothing to commit (create/copy files and use "git add" to track)
```
Il n'y a rien : le repo est vide.

### Les commits
Git ne sauvegarde pas automatiquement tous les fichiers, cela poserai de nombreux problèmes (si un mot de passe est contenu dans un fichier par exemple). Il faut donc les ajouter a la main et dire a git "suis ce fichier" ou "update l'état de ce fichier". De manière générale ces modifications on un but, on les regroupe donc dans un "commit". Un commit n'est rien d'autre qu'un fichier sauvegardé par git disant par exemple : "fichier A, ligne 3 tu remplaces 'a' par 'à', fichier B, ligne 785, tu ajoute ce bout de code, etc".

Pour se simplifier la vie, on peut ajouter un commentaire a ces modifications : c'est le *commit message*.

Quand on travaille avec git, le raisonnement est donc le suivant :
1. Je fais une modification
2. Je test
    - Si ça ne marche pas : je vais a `1`
    - Si ça marche : je vais a `3`
3. Je commit
4. Je vais a `1`

Les commits ont donc pour but d'être de petites modifications cohérentes de code permettant de comprendre, sans même lire le code, le raisonnement du développeur.

Git gere les fichiers en trois étapes :
- Untracked : le fichier n'est pas encore suivi par git
- Tracked : le fichier à déjà été commit mais il a de nouvelles modifications
- Staged : git est prêt à le commit

Dans les cas untracked et tracked, il faut d'abord ajouter les fichiers a la *staging area* pour pouvoir ensuite le commit. De la même manière, on peut untrack un fichier que l'on ne veut pas commit.

### first commit
Nous allons :
- Ajouter un fichier
- L'ajouter a la staging area
- Le commit avec le message "first commit"

```
$ echo "a" > fichier1
$ git status
On branch master

No commits yet

Untracked files:
  (use "git add <file>..." to include in what will be committed)
	fichier1

nothing added to commit but untracked files present (use "git add" to track)
$ git add fichier1
$ git status
On branch master

No commits yet

Changes to be committed:
  (use "git rm --cached <file>..." to unstage)
	new file:   fichier1

$ git commit -m "first commit"
[master (root-commit) 8a679f5] first commit
 1 file changed, 1 insertion(+)
 create mode 100644 fichier1
$ git status
On branch master
nothing to commit, working tree clean
```

Comme on peut le remarquer, git propose automatiquement de stage (`git add <file>...`) et unstage (`git rm --cached <file>...`) les fichiers, la seule commande à retenir est `git status` (sinon `git help` donne une aide détaillée).

On peut maintenant voir l'historique des commits :

```
$ git log
commit 8a679f567c0d640c09b0020ef2974ae69afb861e (HEAD -> master)
Author: ...
Date:   ...

    first commit
```
On voit bien qu'il a un seul commit avec pour message "first commit". Si on veut l'observer plus en détail, il faut utiliser le *commit hash* qui est le code un peu moche en face de "commit" (en réalité les premiers caractères suffisent) :
```
$ git show 8a679f
commit 8a679f567c0d640c09b0020ef2974ae69afb861e (HEAD -> master)
Author: ...
Date:   ...

    first commit

diff --git a/fichier1 b/fichier1
new file mode 100644
index 0000000..7898192
--- /dev/null
+++ b/fichier1
@@ -0,0 +1 @@
+a
```
Ce n'est pas très lisible par un humain mais git lui peut facilement appliquer ce fichier sur un autre repo pour partager facilement les modifications.

## Sauvegarder et partager ses projets

### Un peu de théorie
Quand on travaille seul et en local, git n'a pas un intérêt énorme, il ne permet que de faire une gestion des modifications et de voir quand est-ce que l'on a travaillé. Mais supposons que l'on veuille partager des fichiers, git devient vraiment utile. En plus de stocker les fichiers sur un serveur, les autres personnes pourrons consulter les messages de commits afin de voir quels ont été les modifications.

Le workflow se complexifie un peu mais cela permet par exemple de sauvegarder et de distribuer sur plusieurs serveurs le même travail ce qui est bien plus complexe avec des drives classiques.

De plus, de nombreuses solutions gratuites de stockage git sont disponible en ligne et en open source. Il y a par exemple gitlab et github.

Il faut premièrement se créer un compte sur une de ces plateformes puis créer son premier repo. Ensuite il faut créer un repo en local (s'il n'existe pas déjà). Il faut enfin lier les deux repos. On peut ensuite synchroniser les repo quand on le souhaite. 

### Un peu de pratique

#### Comptes gitlab et github

Ici nous allons commencer par se créer un compte github et gitlab. Ensuite il faut se créer un repo sur chacun de ces  deux sites. Pour github, il suffit de cliquer sur *new*, d'ajouter un nom et de faire `create repository`. Sur gitlab il faut choisir blank project de lui donner son nom et idem `create repository`.

Vous devriez avoir une fenêtre semblable pour les deux repos :

**Git global setup**
```
git config --global user.name "???"
git config --global user.email "???"
```

**Create a new repository**
```
git clone git@gitlab.com:???/???.git
cd tmp
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

```

**Push an existing folder**
```
cd existing_folder
git init
git remote add origin git@gitlab.com:???/???.git
git add .
git commit -m "Initial commit"
git push -u origin master
```

**Push an existing Git repository**

```
cd existing_repo
git remote rename origin old-origin
git remote add origin git@gitlab.com:???/???.git
git push -u origin --all
git push -u origin --tags
```

#### Configuration locale

Il faut donc configurer son nom et son adresse email comme celle renseignée sur gitlab et github. Puis nous allons faire la dernière étape pour synchroniser notre repo git sur ces deux serveurs.

Nous allons donc ajouter les deux *remote* a notre repo git : Une *remote* désigne un repo git distant qui vas récupérer les modifications. Pour cela il suffit de faire les commandes suivantes :
```
$ git remote add github <remote github>
$ git remote add gitlab <remote gitlab>
```
Ou les remotes sont donnée précédemment sur chacune des pages : `git@git???.com:???/???.git`.

Maintenant il faut envoyer nos modifications au serveur. Trop simple ? Et oui, n'importe qui pourrait faire la meme chose et nous voler notre précieux travail !

#### Clés SSH

Il faut donc s'authentifier au près de gitlab et github pour pouvoir travailler avec les remotes. Cela ce fait en trois étapes :

1. Génération de clés
2. Ajout local de la clé privé
3. Ajout distant de la clé publique

##### Génération de clés
Il suffit de faire `ssh-keygen` et de suivre les instructions de l'utilitaire. Il va créer deux fichiers `id_rsa` (la clé privée) et `id_rsa.pub` (la clé publique).

##### Ajout local de la clé privé
Il faut ensuite dire a son ordinateur que l'on a une clé de sécurité :
```
$ eval $(ssh-agent)
$ ssh-add path/to/private/key
```

##### Ajout distant de la clé publique
Maintenant que notre ordinateur est configuré =, il faut configurer gitlab et github : Sur chacun des serveurs allez dans settings puis sécurité / clé ssh et collez le contenu de la clé publique.

### On push !
Maintenant il ne reste plus qu'a envoyer ses modifications aux deux serveurs. Pour cela il faut `push` :
```
$ git push -u github master
$ git push -u gitlab master
```
On peut maintenant aller voir les modifications sur les deux pages web, on devrait voir tous nos fichiers.

### On clone

Supposons que l'on veuille modifier le repo depuis un autre ordi, il faudra faire les étapes suivantes :
1. Cloner le repo `git clone <url>`
2. Modifier des fichiers
3. Commit les changements
4. Push les commits

Enfin si quelqu'un d'autre a push ses modifications, il ne faut pas oublier de faire un `git pull` avant de faire des  commits.

## La gestion de projet a plusieurs

On se dit bien que la modification de nombreuses personnes peut poser des problèmes si l'on veut pouvoir a la fois résoudre de bugs, tester de nouvelles approches et conserver une version toujours fonctionnelle. Heureusement git nous permet de créer des branches qui permettent de faire toutes ces choses a la fois. Depuis le panneau gitlab (par exemple) créez une "issue" : cela définit une chose à faire, un bug à résoudre ou une nouvelle chose à tester pour cet example le contenu n'a pas d'importance mais il faudrait expliquer comment reproduire le bug ou des approches envisagées.

Une fois votre issue crée, vous pouvez créer une branche associée à sa résolution : dans cette branche, rien ne peux marcher, pas de problème car vous ne travaillez pas sur la branche principale. Pour créer une branche à partir d'une issue, rien de plus simple, gitlab vous le propose `Create merge request`. Cela vous ouvre directement la page de la `merge request` : quand vos modifications seront finales, vous pourrez proposer au groupe votre modification et ainsi les voir ajoutées à la branche principale.

Sur votre terminal, il faut juste se raccrocher à la branche que vous avez créé pour faire les modifications sur celle-ci. Pour cela il suffit de tapper :
```
$ # on se met a jour
$ git fetch
$ # et on passe sur la bonne branche
$ git checkout -b <branch> <remote>/<branch>
```

## Se simplifier la vie

Si vous arrivez bien à faire tout ce qui est au-dessus, vous pouvez passer a la vitesse supérieure : utiliser le terminal H-24 pour chaque action peut être long... Heureusement de nombreuses extensions pour vos IDEs sont disponibles (Visual code, Jetbrains, ou même VIM) ! Allez donc chercher sur internet les meilleures extensions pour votre IDE mais souvenez-vous que tous les programmes du monde ne remplacerons pas votre cerveau, si vous ne maitrisez pas la logique de git, vous allez continuellement avoir des problèmes lors de son utilisation ce qui vas vous décourager de l'utiliser hors ce qui fait la puissance de git est bien l'atomicité des commits : un commit doit ne contenir que quelques lignes de code afin de le rendre le plus compréhensible possible.
