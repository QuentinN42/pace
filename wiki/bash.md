# Tuto bash

## Bash quésaco ?
Bash est un langage permettant d'interagir avec le système d'exploitation ainsi que ses applications. Grace a un terminal, on peut installer des programmes, les utiliser, lancer des applications graphiques, éditer des fichiers, créer des archives, etc.

Mais tout cela est faisable via interface graphique... Pourquoi diable s'embêter avec des lignes de commandes impossible à retenir... Le point central de la ligne de commande est son fonctionnement : On peut lancer une commande et suivant son résultat faire des actions. Cela permet d'automatiser très rapidement de nombreuses taches. On peut aussi rediriger la sortie d'un programme vers l'entrée d'un autre. De plus la plupart des ordinateurs sont des serveurs sur lesquels il n'y a pas d'interface graphique. On s'y connecte donc via ligne de commande et toutes les interactions avec le serveur sont réalisées via celle-ci.

Exemple :
Je veux compter le nombre de lignes contenant "Bonjour" dans le fichier `Hello.txt`. On peut facilement compter le nombre d'occurrences avec le bloc-note mais pas le nombre de lignes...
Via bash il suffit de taper :
```
cat Hello.txt | grep Bonjour | wc -l
```
Oulala... Beaucoup de mots compliqué ! Allons-y doucement :
1. Lire le fichier :
   `cat` permet de lire un fichier et le fichier à lire est `Hello.txt`.
2. On veut chercher Bonjour :
   `|` permet de rediriger la sortie de la commande précédente vers l'entré de la commande actuelle.
   `grep` est un programme permettant de faire des recherches, ici on cherche `Bonjour`
3. On veut compter le nombre de lignes
   Ici de la même manière `|` redirige les lignes qui contiennent `Bonjour`.
   `wc` est un programme qui permet de compter plein de choses.
   Ici on veut le nombre de lignes (`-l` pour line en anglais).

Bon bien entendu ici l'exemple n'est pas très utile mais vous voyez le principe, on peut faire des choses bien plus complexes comme lancer des serveurs, automatiquement relancer des programmes s’il y a des erreurs, envoyer un mail avec les erreurs dedans, résoudre des bugs, etc.

Aller on tente de faire quelques trucs. Mais le meilleur moyen d'apprendre est de pratiquer donc lançons un terminal et testons les commandes afin de se familiariser avec cet outil.

## Installation
Si vous êtes sur linux ou sur mac, vous n'avez rien à faire. Passez à la suite.

Si vous utilisez windows 10, suivez les étapes suivantes :
1. Installer WSL :
   a. Dans un powershell en mode admin tapez :
   ```
   dism.exe /online /enable-feature /featurename:Microsoft-Windows-Subsystem-Linux /all /norestart
   ```
   b. Allez dans vos paramètres et faites les mises a jour (redémarrez si nécessaire).
   c. Dans un powershell en mode admin tapez :
   ```
   dism.exe /online /enable-feature /featurename:VirtualMachinePlatform /all /norestart
   ```
   d. Redémarrez windows.
   e. [Téléchargez et installez](https://wslstorestorage.blob.core.windows.net/wslblob/wsl_update_x64.msi) le kernel
   linux pour windows.

2. Installer Ubuntu :
   a. Ouvrez le microsoft store.
   b. Chercher "Ubuntu 20.04".
   c. L'installer.

3. Set up Ubuntu :
   a. Lancer le programme nommé Ubuntu.
   b. Attendez la fin de l'installation.
   c. Ajoutez-vous un nom de compte et un mot de passe.

Vous pouvez retrouver le tutoriel complet (en anglais) sur
[le site de microsoft](https://docs.microsoft.com/en-us/windows/wsl/install-win10).

## Les bases

### Présentation
Lancez le terminal bash. Vous devriez voir quelque chose de ce type :
```shell
XXX@YYY ~$ 
```
Cette ligne s'appelle le `PS1`.
Il y a 4 informations importantes à voir ici :
- `XXX` : Votre nom d'utilisateur.
- `YYY` : Le nom de la machine sur laquelle vous êtes.
- `~` : L'emplacement sur la machine.
- `$` : Le type de compte.
  Cette ligne peut être customisée a vos souhaits, nous verrons cela plus tard.

#### L'utilisateur
Contrairement a Windows, linux utilise de nombreux utilisateurs pour isoler les services. L'utilisateur "Admin" s'appelle `root` et contrairement a tout les autres utilisateurs, le signe qui le repère est le
`#`.

Par habitude, si un utilisateur normal utilise une commande on le note :
```
$ cmd
Retour de cmd
```
Si root utilise une commande, on le note :
```
# cmd
Retour de cmd en mode admin
```

#### La machine
L'invite de commande est très pratique car normalisé entre tous les systèmes, il est souvent pratique de savoir sur quel serveur on est en train de travailler.


#### Le chemin
Étant données que les commandes s'exécutent en fonction de l'emplacement sur la machine il est toujours important de savoir ou l'on est, on voit ça tout de suite.


### Le fonctionnement

Cela peut vous paraitre bien plus compliqué qu'une interface graphique ou il y a des boutons mais je vous assure qu'avec un peu de pratique, on est bien plus productif avec cet outil.

Le fonctionnement est très simple :
1. J'écris une commande
2. J'appuie sur `ENTER`
3. La machine execute la commande
4. La machine écrit le résultat dans le terminal
5. La machine me rend la main

À n'importe quel moment, si je veux tout arrêter et revenir a l'étape 1, il faut envoyer `CTRL + C` au terminal. NB : `CTRL + C` n'est pas utilisé pour copier mais pour arrêter une commande. Si on veut copier/coller il faut utiliser `CTRL + MAJ + C`/`CTRL + MAJ + V` (sur windows il faut l'activer dans les paramètres).


### Commandes de base

Chaque commande se fait de la manière suivante : Une commande est un mot sans espace, a la suite de ce mot, on peut lui ajouter des arguments (par exemple un fichier ou du texte, etc) : Un argument est quelque chose qui modifie le fonctionnement de la commande il y en a plusieurs types montrés ci-dessous :
```
$ cmd arg1 -abc --param1 --param2=5 arg2
```
Ici `arg1` et `arg2` sont des entrées du programme, le `-abc` consiste à passer les trois paramètres  `a`, `b` et `c` au programme enfin `--param1` et `--param2=5` sont des paramètres passés au programme (ici on donne la valeur `5` à `param2`).


Tentons quelques commandes :

#### echo : Hello world
La commande `echo` permet d'écrire du texte dans la ligne de commande :
```
$ echo "Hello world"
Hello world
```

#### PWD : Où suis-je ?
Tapez
```
$ pwd
```
Vous devriez voir quelque chose du type `/home/XXX/` : C'est l'endroit ou vous êtes actuellement.

#### ls : Qu'y a-t-il dans ce dossier ?
Tapez
```
$ ls
a	   b    c    d ...
...
```
Vous dévirez obtenir quelque chose de ce style. Chacun des éléments suivants sont des dossiers ou des fichiers.

On peut voir plus d'informations en utilisant de nombreux arguments :
```
$ ls -l
```
Vas afficher les elements sous forme de liste avec de nombreuses informations.

#### cd : on se déplace !
`cd` est une commande qui permet de changer de dossier courant. Faites un `ls` et choisissez un dossier (`Downloads` par exemple).
Tapez :
```
$ cd MonDossier
```
Regardez ou vous êtes grace a `pwd`. Si vous voulez revenir en arrière utiliser `..` comme nom de dossier. Par exemple cette commande ne vous fait pas bouger :
```
$ cd a/../b/../
```
Il y a deux manières de repérer les dossiers :
- Chemin ABSOLU.
  Depuis la racine de l'ordinateur
  On commence par `/`.
- Chemin RELATIF.
  Depuis le dossier courant
  On commence par `./`.
  Par défaut `cd` utilise les chemins relatifs mais on peut le forcer avec l'utilisation de `./` ou `/`.
  Les commandes `cd a` et `cd ./a` sont équivalentes mais ne font pas la même chose que `cd /a`.

Exercice :
Ou vous envoie la commande :
```
$ cd a/b/../c/../../../d
```
<details>
  <summary>Réponse</summary>
  <code>a/b/../ -> a/</code><br>
  <code>a/b/../c/../ -> a/</code><br>
  <code>a/b/../c/../../ -> ./</code><br>
  <code>a/b/../c/../../../d -> ../d</code>
</details>


#### mkdir : make directory
Commençons par créer un dossier pour pouvoir travailler :
```
$ mkdir tutos-prog
```
Déplacez-vous dedans.
<details>
  <summary>Réponse</summary>
  <code>cd tutos-prog</code>
</details>

#### touch : On commence à écrire des fichiers
Dans ce dossier nous allons ajouter 3 fichiers : `fichier1`, `fichier2` et `fichier3`.
```
$ touch fichier1 fichier2 fichier3
```
NB : Pour linux, l'extension n'a pas d'importance, on aurais pus les appellerer `fichierX.txt` mais étant donné que l'on va préciser spécifiquement avec quel programme nous allons ouvrir ce fichier, l'extension n'a pas d'importance.

#### rm : on supprime TOUT !
`rm` permet de supprimer des dossiers, des fichiers, etc. Attention, sur linux il n'y a pas de corbeille, les fichiers sont directement perdus ! Supprimez le fichier 1 :
```
$ rm fichier1
```


#### nano : on écrit des choses
Le programme `nano` est un petit utilitaire minimaliste permettent d'éditer des fichiers via ligne de commande. Éditez le fichier `fichier1` avec nano :
```
$ nano fichier1
```
Normalement vous voyez votre terminal changer pour quelque chose qui resemble a un éditeur. Si vous avez :
```
$ nano fichier1
bash: nano: command not found
```
Passez à la partie "Le gestionnaire de packets" et revenez ici après. Écrivez :
```
ligne 1 
ligne 2
Bonjour
ligne 4
ligne 5
Hello
ligne 7
ligne 8
```
Pour sauvegarder, utilisez `CTRL + O` puis `CTRL + X` pour quitter.

#### Lecture de fichier
Ici on va voir 3 programmes : `cat`, `tail` et `head`.
- `cat` : tout lire
- `tail -n x` : les x dernières lignes
- `head -n x` : les x premieres lignes

Essayez de vous familiariser avec ces commandes.


### Le gestionnaire de packets

#### Update
Commençons par regarder si des mises ajour sont disponibles :
```
# apt update
```
Oh mais c'est `#` et pas `$`, comment j'exécute cette commande ? Il faut spécifier que root (admin) doit exécuter cette commande. Pour cela utiliser `sudo` :
```
$ sudo apt update
```
Vous devriez voir `X packages can be upgraded.` a la fin de la sortie.

#### Upgrade
Faisons les mises à jour (commande exécuté en tant que root) :
```
# apt upgrade
```
<details>
  <summary>Réponse</summary>
  <code>sudo apt upgrade</code>
</details>
Et voila, le système est a jour, on vas pouvoir installer des programmes !

#### Installation
Essayons d'installer `wget`, un programme pour télécharger des pages web.
```
$ apt search wget
Sorting... Done
Full Text Search... Done

...

wget/focal 1.20.3-1ubuntu1 amd64
  retrieves files from the web

...

```
Le programme existe bien, super, installons le :
```
# apt install wget
```
On peut maintenant l'utiliser :
```
$ wget https://upload.wikimedia.org/wikipedia/commons/5/53/GNU_and_Tux.svg
...
---------- --:--:-- (-.-- MB/s) - 'GNU_and_Tux.svg' saved [------]
$ ls
...
GNU_and_Tux.svg
...
```

### Les modifieurs

Commençons à voir des choses plus intéressantes : Les 10 modifieurs suivants permettent de lier des commandes :
#### `|`
Permet de rediriger la sortie d'un programme vers un autre par exemple :
```
$ cat fichier1 | sort
```
Print le contenu de fichier1 trié par ordre alphabétique.

#### `$()`
Permet d'exécuter une commande dans une commande :
```
$ echo "Fichier 1 : \n---\n$(cat fichier1)\n---"
Fichier 1 : 
---
ligne 1 
ligne 2
Bonjour
ligne 4
ligne 5
Hello
ligne 7
ligne 8
---
```
On s'en sert souvent pour exécuter un programme sur la sortie d'un autre un peu comme `|`. Par exemple :
```
$ cat $(ls)
```
Permet de lire tous les fichiers dans le dossier courant.

#### `&`
Permet d'exécuter deux commandes en simultané :
```
$ mkdir a & ls
[1] 2843
GNU_and_Tux.svg  a  fichier1  fichier2  fichier3
[1]+  Done                    mkdir a
```
Ici `[1] 2843` donne le PID du process qui tourne en arrière-plan et `[1]+  Done` veut dire que le process 1 s'est bien
terminé.

#### `;`
Permet d'exécuter deux commandes l'une après l'autre.
```
$ mkdir a ; ls
GNU_and_Tux.svg  a  fichier1  fichier2  fichier3
```
Contrairement a `&`, le terminal attend le retour de la commande 1 avant l'envoi de la seconde commande.

#### `||`
Permet d'exécuter deux commandes l'une après l'autre si la première échoue.
```
$ ls || echo "Oups..."
GNU_and_Tux.svg  a  fichier1  fichier2  fichier3
$ ddd || echo "Oups..."
bash: ddd: command not found
Oups
```

#### `&&`
Permet d'exécuter deux commandes l'une après l'autre si la première réussit.
```
$ ls && echo "Yes."
GNU_and_Tux.svg  a  fichier1  fichier2  fichier3
Yes.
$ ddd && echo "Yes."
bash: ddd: command not found
```
Cette commande est très utilisée pour enchainer des commandes : `c1 && c2 && c3` si il y a un problème quelque part, tout s'arrête !


#### `>`
Permet de rediriger le retour d'une commande vers un fichier : supprime et remplace le fichier.
```
$ ls > fichier2
```

#### `2>`
Permet de rediriger les erreurs d'une commande vers un fichier : supprime et remplace le fichier.
```
$ zzz 2> fichier2
```

#### `>>`
Permet de rediriger le retour d'une commande vers un fichier : ajoute à la fin du fichier.
```
$ echo "Fin du ls" >> fichier2
```

#### `{}`
Permet de grouper des commandes.
```
$ {ls && echo "Fin du ls"} > fichier3
```

#### `<`
Permet de rediriger le contenu d'un fichier vers une commande. Très peu utilisé mais indispensable pour les boucles for et while :
```
$ while IFS= read -r line; do echo "fichier3: $line"; done < fichier3
fichier3: GNU_and_Tux.svg
fichier3: a
fichier3: fichier1
fichier3: fichier2
fichier3: fichier3
fichier3: Fin du ls
```

#### `<<`
Idem que `<` mais en utilisant l'entré du terminal : Après `<<` on met une chaine de caractères (souvent `EOF` ou `EOS`), quand on donne cette chaine de caractère, la commande est exécutée avec pour entrer tout ce qui est au-dessus :
```
$ while IFS= read -r line; do echo "entree: $line"; done << EOF
Hello
coucou

bye
EOF
entree: Hello
entree: coucou
entree: 
entree: bye
```


### Commandes avancées

Bon maintenant qu'on sait à peu près utiliser le terminal, on va pouvoir s'amuser !

#### tree : un ls plus joli

La commande `tree` permet de voir l'arborescence d'un dossier de manière plus jolie que `ls`. Il faut peut-être l'installer via `apt`, je vous laisse faire et tester.

#### find : on cherche PARTOUT !

Si on veut lister tous les fichiers et dossiers à partir d'un chemin, on utilise `find path`. Par exemple :
```
$ find .
a/
a/b/
a/b/c/file1.txt
a/b/d/
find: cannot access '/.../': Permission denied
a/b/file2.txt
```
Par exemple ici on voit d'un coup qu'il y a deux fichiers texte et un dossier qui ne nous appartient pas.

#### /dev/null : le trou noir

Tous les fichiers dans `/dev/` sont des fichiers spéciaux `/dev/null` ne contient jamais rien : C'est très pratique pour par example masquer des erreurs comme par exemple :
```
$ find . 2>/dev/null
a/
a/b/
a/b/c/file1.txt
a/b/d/
a/b/file2.txt
```
On a masqué l'erreur avec `2>/dev/null`.

#### grep : on filtre

`grep` est une des commandes les plus utilisées avec l'invite de commande : elle filtre la sortie d'un programme. Pour chaque ligne, elle est affichée ssi la ligne contient le filtre (on peut bien entendu faire bien plus de choses avec... cf. Pour en savoir plus). Exemple : On veut trouver ce fameux rendu de TP qui est quelque part sur notre ordi... Mais où ?
```
$ find / 2>/dev/null | grep "TP3.pdf"
...
```


#### sed : on remplace

La commande `sed` permet de remplacer du texte par un autre. Par exemple si on veut remplacer les `l` par des `k` :
```
$ cat fichier1 | sed -e "s/l/k/g"
kigne 1 
kigne 2
Bonjour
kigne 4
kigne 5
Hekko
kigne 7
kigne 8
```
Si on veut supprimer les `ligne` (remplacer par rien du tout) :
```
$ cat fichier1 | sed -e "s/ligne//g"
 1 
 2
Bonjour
 4
 5
Hello
 7
 8
```

#### sort : on trie

Pour trier un fichier par exemple :
```
$ sort fichier1
Bonjour
Hello
ligne 1 
ligne 2
ligne 4
ligne 5
ligne 7
ligne 8
```

#### uniq -c : on compte les occurrencessu

La commande `uniq` permet d'afficher les lignes en un seul exemplaire, l'argument `-c` permet de compter le nombre d'occurrences.

#### wc : on compte d'autres trucs

Cette commande permet de compter de nombreux paramètres d'un fichier :
* `-c` : le nombre de bytes
* `-m` : le nombre de caractères
* `-l` : le nombre de lignes
* `-L` : la plus longue ligne
* `-w` : le nombre de mots


#### shasum : est-ce la meme chose ?

Pour comparer deux fichiers on peut calculer leur [hash](https://fr.wikipedia.org/wiki/Hash). Il y a de nombreux algorithmes de hash dans bash de base, ils s'appellent tous via la meme commande :
```
$ shasum fichier1 fichier2
8fe2c0da2a7a51f9cf1710a019ec4c35563f4a28  fichier1
27c19b10d0ea68a194ee74d7e13932fe16bbf75c  fichier2
```
Les hash sont différents, les fichiers ne sont pas identiques. 

## Pour en savoir plus

De nombreuses ressources peuvent vous aider à apprendre plus de choses :

La quasi-totalité des commandes sont disponibles sur le site web : [linux.die.net](https://linux.die.net/man/1/). Ou vous pouvez voir leur aide en tapant la commande `man [commande]`.

De nombreux autres medias peuvent vous aider à devenir incollable sur bash, je vous mets deux livres :
- [L'art de la ligne de commande](https://github.com/jlevy/the-art-of-command-line/blob/master/README-fr.md).
- [Pour aller plus loin avec la ligne de commande](https://framabook.org/docs/Pour_aller_plus_loin_avec_la_ligne_de_commande/Pour_aller_plus_loin_avec_la_ligne_de_commande_art-libre.pdf).
