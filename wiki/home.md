# Introduction

## L'informatique d'aujourd'hui

Avec l'explosion du numérique, le nombre de serveurs et de programmes ont décuplé durant les dernières années. Pour être toujours plus compétitives et ainsi obtenir un maximum de clients, les entreprise doivent sans cesse innover et mettre a jour leurs programmes afin de satisfaire les demandes de plus en plus complexes de leurs clients.

Dans cette course effrénée a la mise a jour, le temps et la fiabilité du développement devenu une valeur essentielle (devant l'optimisation en temps de calcul et en mémoire). Cependant, la vitesse de développement et sa fiabilité sont souvent des ennemis.

Il faut donc développer des technique et des technologies pour augmenter la fiabilité de ces développements.

## Le DevOps

De par la complexification du metier, un développeur ne fait jamais une application d'un bout a l'autre, il se spécialise dans un domaine et résout des problèmes très spécifiques. Il est donc important que tous les développeurs utilisent les mêmes languages et les même normes d'écriture de code.

De plus, de nombreux outils sont communs a tous les employés d'entreprise du numériques : l'utilisation de git et bash sont essentiel pour collaborer.

Enfin, connaitre les bases des autres metiers du développement et du sysadmin permet de prévenir de nombreux bugs avant leur apparition. L'ensemble de ses connaissances s'inscrivent dans la mentalité DevOps : une connaissance transverse et un développement plus sûr.

## Note aux lecteurs

Ce wiki n'a pas pour objectif d'être figé, si vous voyez que certaines parties ne sont pas très claires n'hésitez pas à les améliorer. Si vous voullez créer un nouveau tuto, contactez-moi a l'adresse [quentin.lieumont@telecom-paris.fr](mailto:quentin.lieumont@telecom-paris.fr).
