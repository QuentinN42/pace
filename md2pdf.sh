#!/usr/bin/env bash

docker run \
    -v="$(pwd)/cover/cover_template.md:/usr/local/lib/python2.7/dist-packages/markdown_to_pdf-0.2.2-py2.7.egg/markdown_to_pdf/cover_template/cover_template.md"\
    -v="$(pwd):/md2pdf" \
    fiware/md2pdf \
    -i /md2pdf/md2pdf.yml -o /md2pdf/documentation.pdf
